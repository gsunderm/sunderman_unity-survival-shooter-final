﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public int numberOfBullets = 1;
    public float timeBetweenBullets = 0.15f;
    public float bounceDuration = 10;
    public float pierceDuration = 10;
    public float range = 100f;
    public float angleBetweenBullets = 10f;


    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    public Image bounceImage;
    public Image pierceImage;

    public GameObject bullet;
    public Transform bulletSpawnAnchor;

    


    float bounceTimer;
    float pierceTimer;
    bool bounce;
    bool piercing;

    public float BounceTimer
    {
        get { return bounceTimer; }
        set { bounceTimer = value; }
    }

    public float PierceTimer
    {
        get { return pierceTimer; }
        set { pierceTimer = value; }
    }

    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();

        bounceTimer = bounceDuration;
        pierceTimer = pierceDuration;
    }


    void Update ()
    {
        if (bounceTimer < bounceDuration)
        {
            bounce = true;
        }
        else
        {
            bounce = false;
        }

        if (pierceTimer < pierceDuration)
        {
            piercing = true;
        }
        else
        {
            piercing = false;
        }

        bounceImage.gameObject.SetActive(bounce);
        bounceImage.gameObject.SetActive(piercing);

        // Add the time since Update was last called to the timer.
        bounceTimer += Time.deltaTime;
        pierceTimer += Time.deltaTime;
        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        if (bounce)
        {
            gunAudio.pitch = Random.Range(1.1f, 1.2f);
        }
        if (piercing)
        {
            gunAudio.pitch = Random.Range(1.0f, 1.1f);
        }
        if (piercing & bounce)
        {
            gunAudio.pitch = Random.Range(0.9f, 1.0f);
        }
        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }

        for (int i = 0; i < numberOfBullets; i++)
        {
            // Make sure our bullets spread out in an even pattern.
            float angle = i * angleBetweenBullets - ((angleBetweenBullets / 2) * (numberOfBullets - 1));
            Quaternion rot = transform.rotation * Quaternion.AngleAxis(angle, Vector3.up);
            GameObject instantiatedBullet = Instantiate(bullet, bulletSpawnAnchor.transform.position, rot) as GameObject;
            instantiatedBullet.GetComponent<Bullet>().piercing = piercing;
            instantiatedBullet.GetComponent<Bullet>().bounce = bounce;
        }
    }
}

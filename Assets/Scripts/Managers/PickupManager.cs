﻿using UnityEngine;
using System.Collections;

public class PickupManager : MonoBehaviour {
	
	public int scoreNeededForExtraBullet = 70;
	public int extraScoreNeededAfterEachPickup = 70;

	public Pickup healthPickup;
	public Pickup bouncePickup;
	public Pickup piercePickup;
	public Pickup bulletPickup;
}
